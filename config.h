/* See LICENSE file for copyright and license details. */
#include <X11/XF86keysym.h>

/* appearance */
static const unsigned int borderpx  = 4;        /* border pixel of windows */
static const unsigned int snap      = 31;       /* snap pixel */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
 static const char *fonts[]          = { "UbuntuMono Nerd Font:size=14" };
/*static const char col_gray1[]       = "#222222";*/
/*static const char col_red[]        = "#924441";  iets minder fel*/

/* LET OP: deze varabelen staan gedefinieerd in .Xresources. */
static char normbgcolor[]           = "#222222";
static char normbordercolor[]       = "#444444";
static char normfgcolor[]           = "#bbbbbb";
static char selfgcolor[]            = "#eeeeee";
static char selbordercolor[]        = "#005577";
static char selbgcolor[]            = "#005577";
static char *colors[][3] = {
       /*               fg           bg           border   */
       [SchemeNorm] = { normfgcolor, normbgcolor, normbordercolor },
       [SchemeSel]  = { selfgcolor,  selbgcolor,  selbordercolor  },
};


/* tagging */
static const char *tags[] = { "", "", "", "", "", "" };

static const Rule rules[] = {
/* switchtag (patch 13.dwm-switchtag-6.2):
   - 0 is default behaviour
   - 1 automatically moves you to the tag of the newly opened application and
   - 2 enables the tag of the newly opened application in addition to your existing enabled tags
*/

	/* class            instance    title                  tags mask     switchtag	isfloating   monitor */
	{ "St",             NULL,       "zsh",                 1 << 0,       1,            0,           -1 },
	{ NULL,             NULL,       "vim",                 1 << 1,       1,            0,           -1 }, 
	{ "firefox",        NULL,       NULL,                  1 << 2,       1,            0,           -1 },
	{ NULL,             NULL,  	"/usr/bin/k9s",        1 << 3,       1,            0,           -1 },
	{ NULL,             NULL,       "Spotify Premium",     1 << 4,       1,            0,           -1 },
	{ "Spotify",        NULL,       NULL,                  1 << 4,       1,            0,           -1 },
	{ NULL,             "spotify",  NULL,                  1 << 4,       1,            0,           -1 },
};

/* layout(s) */
static const float mfact     = 0.50; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 0;    /* 1 means respect size hints in tiled resizals */
#include "layouts.c" /*afkomstig uit dwm-gridmode patch, heeft deze nodig*/
#include "fibonacci.c" /*afkomstig uit dwm-gridmode patch, heeft deze nodig*/
static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "",      dwindle },
	{ "舘",      tile },    
	{ "ﬓ",      monocle },    
	{ "",      grid },
};


/* key definitions */
/*in logische volgorde: */
/*bekijk tag, */
/*activeer/deactiveer tag*/
/*tag het venster*/
/*toggle de tag van het venster*/
#define MODKEY Mod4Mask

#define TAGKEYS(KEY,TAG) \
	{ ControlMask,                  KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY,                       KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask, 		KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
/* #define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } } */

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { NULL }; /*gebruiken we niet. zie sxhkdrc */
static const char *termcmd[]  = { "st", "zsh" };

static Key keys[] = {
	/* modifier                     key        function        argument */
	{ ControlMask,                  XK_backslash, spawn,       {.v = termcmd } },
	{ MODKEY,                       XK_b,      togglebar,      {0} },
	{ MODKEY,	                XK_h,      focusstack,     {.i = -1 } },
	{ MODKEY,	                XK_k,      focusstack,     {.i = -1 } },
	{ MODKEY,          	        XK_l,      focusstack,     {.i = +1 } },
	{ MODKEY,          	        XK_j,      focusstack,     {.i = +1 } },
	{ MODKEY,                       XK_o,      inplacerotate,  {.i = +1} },
	{ MODKEY,                       XK_i,      inplacerotate,  {.i = -1} },
	{ MODKEY,                       XK_equal,  incnmaster,     {.i = +1 } },
	{ MODKEY,                       XK_minus,  incnmaster,     {.i = -1 } },
	{ MODKEY|ControlMask,           XK_h,      setmfact,       {.f = -0.05} },
	{ MODKEY|ControlMask,           XK_l,      setmfact,       {.f = +0.05} },
	{ MODKEY,                       XK_Return, zoom,           {0} },
	{ ControlMask,                  XK_Tab,    focusstack,     {.i = +1 } },
	{ ControlMask|ShiftMask,        XK_Tab,    focusstack,     {.i = -1 } },
	{ 0,	     		        XK_grave,  view,           {0} }, /*switchen naar vorige tag en terug */
	{ MODKEY,          		XK_space,  togglefullscr,     {0} },
	{ 0,	          		XK_F4,     togglefullscr,     {0} },
	{ 0,	          		XF86XK_AudioMicMute,  togglefullscr,     {0} },
	{ ControlMask,                  XK_grave,  view,           {.ui = ~0 } }, /*alle tags laten zien.*/
	{ ControlMask,                  XK_grave,  setlayout,      {.v = &layouts[3]} }, /*grid*/
	{ ControlMask, 	                XK_q,      killclient,     {0} },
	{ MODKEY,                       XK_s,      setlayout,      {.v = &layouts[1]} }, /*tile*/
	{ MODKEY,                       XK_g,      setlayout,      {.v = &layouts[3]} }, /*grid*/
	{ MODKEY,                       XK_d,      setlayout,      {.v = &layouts[0]} }, /*dwindle*/
	{ MODKEY,                       XK_f,      setlayout,      {.v = &layouts[2]} }, /*fullscreen*/
	{ MODKEY|ControlMask,           XK_k,  	   focusmon,       {.i = -1 } },
	{ MODKEY|ControlMask,           XK_j,      focusmon,       {.i = +1 } },
	{ MODKEY|ShiftMask,           	XK_k,      tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask, 		XK_j,      tagmon,         {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_minus,  setgaps,        {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_equal,  setgaps,        {.i = +1 } },
	/*{ MODKEY|ShiftMask,             XK_equal,  setgaps,        {.i = 0  } },*/
	TAGKEYS(                        XK_1,                      0)
	TAGKEYS(                        XK_2,                      1)
	TAGKEYS(                        XK_3,                      2)
	TAGKEYS(                        XK_4,                      3)
	TAGKEYS(                        XK_5,                      4)
	TAGKEYS(                        XK_6,                      5)
	TAGKEYS(                        XK_7,                      6)
	TAGKEYS(                        XK_8,                      7)
	TAGKEYS(                        XK_9,                      8)
	{ MODKEY|ShiftMask,             XK_r,      quit,           {0} }, /*stop dwm*/
        { MODKEY|ShiftMask,             XK_x,      xrdb,           {.v = NULL } },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         ControlMask,    Button3,        winview,        {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
	{ ClkStatusText,        0,              Button1,        sigdwmblocks,   {.i = 1} },
	{ ClkStatusText,        0,              Button2,        sigdwmblocks,   {.i = 2} },
	{ ClkStatusText,        0,              Button3,        sigdwmblocks,   {.i = 3} },
	{ ClkStatusText,        0,              Button4,        sigdwmblocks,   {.i = 4} },
	{ ClkStatusText,        0,              Button5,        sigdwmblocks,   {.i = 5} },
	{ ClkStatusText,        ShiftMask,      Button1,        sigdwmblocks,   {.i = 6} },
};

/* gaps */
static const int gappx = 0;

/* dwmc */

void
setlayoutex(const Arg *arg)
{
	setlayout(&((Arg) { .v = &layouts[arg->i] }));
}

void
viewex(const Arg *arg)
{
	view(&((Arg) { .ui = 1 << arg->ui }));
}

void
viewall(const Arg *arg)
{
	view(&((Arg){.ui = ~0}));
}

void
toggleviewex(const Arg *arg)
{
	toggleview(&((Arg) { .ui = 1 << arg->ui }));
}

void
tagex(const Arg *arg)
{
	tag(&((Arg) { .ui = 1 << arg->ui }));
}

void
toggletagex(const Arg *arg)
{
	toggletag(&((Arg) { .ui = 1 << arg->ui }));
}

void
tagall(const Arg *arg)
{
	tag(&((Arg){.ui = ~0}));
}

/* signal definitions */
/* signum must be greater than 0 */
/* trigger signals using `xsetroot -name "fsignal:<signame> [<type> <value>]"` */
static Signal signals[] = {
	/* signum           function */
	{ "focusstack",     focusstack },
	{ "setmfact",       setmfact },
	{ "togglebar",      togglebar },
	{ "incnmaster",     incnmaster },
	{ "togglefloating", togglefloating },
	{ "focusmon",       focusmon },
	{ "tagmon",         tagmon },
	{ "zoom",           zoom },
	{ "view",           view },
	{ "viewall",        viewall },
	{ "viewex",         viewex },
	{ "toggleview",     view },
	{ "toggleviewex",   toggleviewex },
	{ "tag",            tag },
	{ "tagall",         tagall },
	{ "tagex",          tagex },
	{ "toggletag",      tag },
	{ "toggletagex",    toggletagex },
	{ "killclient",     killclient },
	{ "quit",           quit },
	{ "setlayout",      setlayout },
	{ "setlayoutex",    setlayoutex },
};



